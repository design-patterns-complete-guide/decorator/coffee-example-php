<?php
// Step 1: Define the Coffee interface (Component)
interface Coffee {
    public function cost();
}
// Step 2: Create a Concrete Component (BasicCoffee)
class BasicCoffee implements Coffee {
    public function cost() {
        return 5; // Basic coffee costs $5
    }
}
// Step 3: Create an abstract Decorator class
abstract class CoffeeDecorator implements Coffee {
    protected $coffee;

    public function __construct(Coffee $coffee) {
        $this->coffee = $coffee;
    }
    public function cost() {
        return $this->coffee->cost();
    }
}
// Step 4: Create Concrete Decorator classes
class MilkDecorator extends CoffeeDecorator {
    public function cost() {
        return parent::cost() + 2; // Add the cost of milk ($2)
    }
}
class SugarDecorator extends CoffeeDecorator {
    public function cost() {
        return parent::cost() + 1; // Add the cost of sugar ($1)
    }
}
// Step 5: Client code
$coffee = new BasicCoffee();
$coffeeWithMilk = new MilkDecorator($coffee);
$coffeeWithMilkAndSugar = new SugarDecorator($coffeeWithMilk);
echo "Cost of Basic Coffee: $" . $coffee->cost() . PHP_EOL;
echo "Cost of Coffee with Milk: $" . $coffeeWithMilk->cost() . PHP_EOL;
echo "Cost of Coffee with Milk and Sugar: $" . $coffeeWithMilkAndSugar->cost() . PHP_EOL;